﻿using UnityEngine;
using System.Collections;

public class PlayerHitEnemy : MonoBehaviour {

    public bool CanHit = true;
    protected Animator animator;

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (CanHit == true && Input.GetMouseButtonDown(0))
        {
            CanHit = false;
            animator.Play("SliceKnife");
            StartCoroutine(CanHitAgain());
        }
    }

    IEnumerator CanHitAgain()
    {
        yield return new WaitForSeconds(0.305f);
        CanHit = true;
    }

	void OnTriggerEnter(Collider col)
    {
        if(CanHit == true)
        {
            return;
        }
        if (col.tag == "EnemyCharacter")
        {
            GameObject.Destroy(col.transform.parent.gameObject);
        }
    }
}
