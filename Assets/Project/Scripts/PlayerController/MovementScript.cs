﻿using UnityEngine;
using System.Collections;

public class MovementScript : MonoBehaviour
{
    Vector3 velocity;
    public float speedFactor;
    public float gravity = 20.0F;
	public Camera referencedCamera;
    private Vector3 moveDirection = Vector3.zero;
    PlayerActions actions;
    CharacterController controller;
    public bool boobleHead;
    // Use this for initialization
    void Start()
    {
        actions = PlayerActions.CreateWithDefaultBindings();
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (controller.isGrounded)
        {
			var forward = referencedCamera.transform.TransformDirection(Vector3.forward);
			forward.y = 0;
			forward = forward.normalized;
			var right  = new Vector3(forward.z, 0, -forward.x);

			moveDirection  = (actions.Move.X * right  + actions.Move.Y * forward);
			moveDirection *= speedFactor;
        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
        if (actions.Move.Vector != Vector2.zero && boobleHead)
        {
            transform.GetChild(0).transform.position = new Vector3(transform.position.x, transform.position.y + Mathf.PingPong(Time.time *0.8f, 0.2f), transform.position.z);
        }
    }
}