﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EnemyPlayerHit : MonoBehaviour {

    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "PlayerOne")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            Debug.Log("Player dead");
        }
    }
}
