﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class OnTriggerEvent : MonoBehaviour {

    public UnityEvent onEnter;

    public void OnTriggerEnter(Collider col)
    {
        if(onEnter != null)
        {
            onEnter.Invoke();
            Invoke("ReloadGame", 0.5f);
        }
    }

    public void ReloadGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
